"use strict";
/*jslint browser: true, nomen: true*/
/*global define*/

define([
  "./playground",
  "./title",
  "./intro",
  "./overview",
  "./election",
  "./replication",
  "./conclusion"
], function (
  playground,
  title,
  intro,
  overview,
  election,
  replication,
  conclusion
) {
  return function (player) {
    // player.frame("playground", "Bac à sable", playground);
    player.frame("home", "Accueil", title);
    player.frame("intro", "Qu'est-ce qu'un consensus distribué ?", intro);
    player.frame("overview", "Vue d'ensemble du protocole", overview);
    player.frame("election", "Election de Leader", election);
    player.frame("replication", "Réplication de Log", replication);
    player.frame("conclusion", "Autres ressources", conclusion);
  };
});
