"use strict";
/*jslint browser: true, nomen: true*/
/*global define*/

define([], function () {
  return function (frame) {
    var player = frame.player(),
      layout = frame.layout(),
      model = function () {
        return frame.model();
      },
      client = function (id) {
        return frame.model().clients.find(id);
      },
      node = function (id) {
        return frame.model().nodes.find(id);
      },
      cluster = function (value) {
        model()
          .nodes.toArray()
          .forEach(function (node) {
            node.cluster(value);
          });
      },
      wait = function () {
        var self = this;
        model().controls.show(function () {
          self.stop();
        });
      },
      subtitle = function (s, pause) {
        model().subtitle = s + model().controls.html();
        layout.invalidate();
        if (pause === undefined) {
          model().controls.show();
        }
      },
      clear = function () {
        subtitle("", false);
      },
      removeAllNodes = function () {
        model()
          .nodes.toArray()
          .forEach(function (node) {
            node.state("stopped");
          });
        model().nodes.removeAll();
      };

    //------------------------------
    // Title
    //------------------------------
    frame
      .after(0, function () {
        model().clear();
        layout.invalidate();
      })
      .after(500, function () {
        frame.model().title =
          '<h2 style="visibility:visible">Réplication de log</h1>' +
          "<br/>" +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(200, wait)
      .indefinite()
      .after(500, function () {
        model().title = "";
        layout.invalidate();
      })

      //------------------------------
      // Cluster Initialization
      //------------------------------
      .after(300, function () {
        model().nodes.create("A");
        model().nodes.create("B");
        model().nodes.create("C");
        cluster(["A", "B", "C"]);
        layout.invalidate();
      })
      .after(500, function () {
        model().forceImmediateLeader();
      })

      //------------------------------
      // Overview
      //------------------------------
      .then(function () {
        subtitle(
          "<h2>Une fois que nous avons un leader élu, nous devons répliquer toutes les modifications apportées à notre système sur tous les noeuds.</h2>",
          false
        );
      })
      .then(wait)
      .indefinite()
      .then(function () {
        subtitle(
          "<h2>Ceci est réalisé en utilisant le même message <em>Ajout d'entrées</em> que celui utilisé pour les pulsations</h2>",
          false
        );
      })
      .then(wait)
      .indefinite()
      .then(function () {
        subtitle("<h2>Passons en revue le processuss.</h2>", false);
      })
      .then(wait)
      .indefinite()

      //------------------------------
      // Single Entry Replication
      //------------------------------
      .then(function () {
        model().clients.create("X");
        subtitle(
          "<h2>Tout d'abord, un client envoie un changement au leader.</h2>",
          false
        );
      })
      .then(wait)
      .indefinite()
      .then(function () {
        client("X").send(model().leader(), "SET 5");
      })
      .after(model().defaultNetworkLatency, function () {
        subtitle("<h2>Le changement est ajouté au log du leader...</h2>");
      })
      .at(model(), "appendEntriesRequestsSent", function () {})
      .after(model().defaultNetworkLatency * 0.25, function (event) {
        subtitle(
          "<h2>...puis la modification est envoyée aux followers à la prochaine pulsation.</h2>"
        );
      })
      .after(1, clear)
      .at(model(), "commitIndexChange", function (event) {
        if (event.target === model().leader()) {
          subtitle(
            "<h2>Une entrée est validée une fois que la majorité des followers en accusent réception...</h2>"
          );
        }
      })
      .after(model().defaultNetworkLatency * 0.25, function (event) {
        subtitle("<h2>...et une réponse est envoyée au client.</h2>");
      })
      .after(1, clear)
      .after(model().defaultNetworkLatency, function (event) {
        subtitle(
          "<h2>Maintenant, envoyons une commende pour incrémenter la valeur de 2.</h2>"
        );
        client("X").send(model().leader(), "ADD 2");
      })
      .after(1, clear)
      .at(model(), "recv", function (event) {
        subtitle("<h2>Notre valeur est maintenant 7.</h2>", false);
      })
      .after(1, wait)
      .indefinite()

      //------------------------------
      // Network Partition
      //------------------------------
      .after(1, function () {
        removeAllNodes();
        model().nodes.create("A");
        model().nodes.create("B");
        model().nodes.create("C");
        model().nodes.create("D");
        model().nodes.create("E");
        layout.invalidate();
      })
      .after(500, function () {
        node("A").init();
        node("B").init();
        node("C").init();
        node("D").init();
        node("E").init();
        cluster(["A", "B", "C", "D", "E"]);
        model().resetToNextTerm();
        node("B").state("leader");
      })
      .after(1, function () {
        subtitle(
          "<h2>Raft peut même rester cohérent face à une partition du réseau.</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        subtitle(
          "<h2>Ajoutons une partition pour séparer A et B de C, D et E</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        model().latency("A", "C", 0).latency("A", "D", 0).latency("A", "E", 0);
        model().latency("B", "C", 0).latency("B", "D", 0).latency("B", "E", 0);
        model().ensureExactCandidate("C");
      })
      .after(model().defaultNetworkLatency * 0.5, function () {
        var p = model().partitions.create("-");
        p.x1 = Math.min.apply(
          null,
          model()
            .nodes.toArray()
            .map(function (node) {
              return node.x;
            })
        );
        p.x2 = Math.max.apply(
          null,
          model()
            .nodes.toArray()
            .map(function (node) {
              return node.x;
            })
        );
        p.y1 = p.y2 = Math.round(node("B").y + node("C").y) / 2;
        layout.invalidate();
      })
      .at(model(), "stateChange", function (event) {
        return event.target.state() === "leader";
      })
      .after(1, function () {
        subtitle(
          "<h2>A cause de la partition, nous avons maintenant deux leaders différents.</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        model().clients.create("Y");
        subtitle(
          "<h2>Ajoutons un nouveau client et essayons de mettre à jours les deux leaders.</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        client("Y").send(node("B"), "SET 3");
        subtitle(
          "<h2>Un des clients essayera de mettre la valeur du noeud B à 3</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        subtitle(
          "<h2>Le noeud B ne peut pas répliquer à la majorité donc son entrée de log reste non validée</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        var leader = model().leader(["C", "D", "E"]);
        client("X").send(leader, "SET 8");
        subtitle(
          "<h2>L'autre client essayera de mettre la valeur du noeud " +
            leader.id +
            " à 8 </h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        subtitle(
          "<h2>Cela réussira car il peut répliquer à la majorité.</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        subtitle("<h2>Maintenant réparons la partition du réseau.</h2>", false);
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        model().partitions.removeAll();
        layout.invalidate();
      })
      .after(200, function () {
        model().resetLatencies();
      })
      .at(model(), "stateChange", function (event) {
        return event.target.id === "B" && event.target.state() === "follower";
      })
      .after(1, function () {
        subtitle(
          '<h2>Le noeud B verra le terme d\'élection plus haut et va "démissioner".</h2>'
        );
      })
      .after(1, function () {
        subtitle(
          "<h2>Les deux noeuds A et B vont revenir en arrière sur leur entrées de log non validées et s'aligner sur le log du leader.</h2>"
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        subtitle(
          "<h2>Notre log est maintenant cohérent au sein du cluster.</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()

      .then(function () {
        player.next();
      });

    player.play();
  };
});
