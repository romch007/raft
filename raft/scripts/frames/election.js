"use strict";
/*jslint browser: true, nomen: true*/
/*global define*/

define([], function () {
  return function (frame) {
    var player = frame.player(),
      layout = frame.layout(),
      model = function () {
        return frame.model();
      },
      client = function (id) {
        return frame.model().clients.find(id);
      },
      node = function (id) {
        return frame.model().nodes.find(id);
      },
      cluster = function (value) {
        model()
          .nodes.toArray()
          .forEach(function (node) {
            node.cluster(value);
          });
      },
      wait = function () {
        var self = this;
        model().controls.show(function () {
          self.stop();
        });
      },
      subtitle = function (s, pause) {
        model().subtitle = s + model().controls.html();
        layout.invalidate();
        if (pause === undefined) {
          model().controls.show();
        }
      };

    //------------------------------
    // Title
    //------------------------------
    frame
      .after(1, function () {
        model().clear();
        layout.invalidate();
      })
      .after(500, function () {
        frame.model().title =
          '<h2 style="visibility:visible">Election de Leader</h1>' +
          "<br/>" +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(200, wait)
      .indefinite()
      .after(500, function () {
        model().title = "";
        layout.invalidate();
      })

      //------------------------------
      // Initialization
      //------------------------------
      .after(300, function () {
        model().nodes.create("A").init();
        model().nodes.create("B").init();
        model().nodes.create("C").init();
        cluster(["A", "B", "C"]);
      })

      //------------------------------
      // Election Timeout
      //------------------------------
      .after(1, function () {
        model().ensureSingleCandidate();
        model().subtitle =
          "<h2>Dans Raft, il existe deux paramètres de délai qui contrôlent les élections.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(model().electionTimeout / 2, function () {
        model().controls.show();
      })
      .after(100, function () {
        subtitle(
          '<h2>Le premier est le <span style="color:green">délai d\'élection</span>.</h2>'
        );
      })
      .after(1, function () {
        subtitle(
          "<h2>Le délai d'élection est le temps qu'un follower attend avant de devenir candidat.</h2>"
        );
      })
      .after(1, function () {
        subtitle(
          "<h2>Le délai d'élection est une valeur aléatoire comprise entre 150 et 300 ms.</h2>"
        );
      })
      .after(1, function () {
        subtitle("", false);
      })

      //------------------------------
      // Candidacy
      //------------------------------
      .at(model(), "stateChange", function (event) {
        return event.target.state() === "candidate";
      })
      .after(1, function () {
        subtitle(
          "<h2>Après l'expiration du délai d'élection, le follower devient candidat et entame un nouveau \"mandat électoral\" ...</h2>"
        );
      })
      .after(1, function () {
        subtitle("<h2>...vote pour lui-même...</h2>");
      })
      .after(model().defaultNetworkLatency * 0.25, function () {
        subtitle(
          "<h2>...et envoie des <em>Demandes de votes</em> aux autres noeuds.</h2>"
        );
      })
      .after(model().defaultNetworkLatency, function () {
        subtitle(
          "<h2>Si le noeud destinataire n'a pas encore voté pendant ce mandat, il vote pour le candidat...</h2>"
        );
      })
      .after(1, function () {
        subtitle("<h2>...et le noeud réinitialise son délai d'élection</h2>");
      })

      //------------------------------
      // Leadership & heartbeat timeout.
      //------------------------------
      .at(model(), "stateChange", function (event) {
        return event.target.state() === "leader";
      })
      .after(1, function () {
        subtitle(
          "<h2>Une fois que le candidat a une majorité de votes, il devient leader.</h2>"
        );
      })
      .after(model().defaultNetworkLatency * 0.25, function () {
        subtitle(
          "<h2>Le leader commence à envoyer des <em>Ajouts d'entrées</em> à ses followers.</h2>"
        );
      })
      .after(1, function () {
        subtitle(
          '<h2>Ces message sont envoyés à des intervalles spécifiés par le <span style="color:red">délai de pulsation</span>.</h2>'
        );
      })
      .after(model().defaultNetworkLatency, function () {
        subtitle(
          "<h2>Les followers répondent alors à chaque message d'<em>Ajout d'entrées</em>.</h2>"
        );
      })
      .after(1, function () {
        subtitle("", false);
      })
      .after(model().heartbeatTimeout * 2, function () {
        subtitle(
          "<h2>Ce mandat d'élection se poursuivra jusqu'à ce qu'un follower cesse de recevoir des pulsations et devienne candidat.</h2>",
          false
        );
      })
      .after(100, wait)
      .indefinite()
      .after(1, function () {
        subtitle("", false);
      })

      //------------------------------
      // Leader re-election
      //------------------------------
      .after(model().heartbeatTimeout * 2, function () {
        subtitle(
          "<h2>Stoppons le chef et assistons à une réélection.</h2>",
          false
        );
      })
      .after(100, wait)
      .indefinite()
      .after(1, function () {
        subtitle("", false);
        model().leader().state("stopped");
      })
      .after(model().defaultNetworkLatency, function () {
        model().ensureSingleCandidate();
      })
      .at(model(), "stateChange", function (event) {
        return event.target.state() === "leader";
      })
      .after(1, function () {
        subtitle(
          "<h2>Le noeud " +
            model().leader().id +
            " est maintenant le leader du mandant " +
            model().leader().currentTerm() +
            ".</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()

      //------------------------------
      // Split Vote
      //------------------------------
      .after(1, function () {
        subtitle(
          "<h2>Exiger une majorité des voix garantit qu'un seul leader peut être élu par mandat.</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        subtitle(
          "<h2>Si deux noeuds deviennent candidats en même temps, un vote partagé peut se produire.</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        subtitle(
          "<h2>Jetons un coup d'oeil à un exemple de vote partagé...</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()
      .after(1, function () {
        subtitle("", false);
        model().nodes.create("D").init().currentTerm(node("A").currentTerm());
        cluster(["A", "B", "C", "D"]);

        // Make sure two nodes become candidates at the same time.
        model().resetToNextTerm();
        var nodes = model().ensureSplitVote();

        // Increase latency to some nodes to ensure obvious split.
        model().latency(
          nodes[0].id,
          nodes[2].id,
          model().defaultNetworkLatency * 1.25
        );
        model().latency(
          nodes[1].id,
          nodes[3].id,
          model().defaultNetworkLatency * 1.25
        );
      })
      .at(model(), "stateChange", function (event) {
        return event.target.state() === "candidate";
      })
      .after(model().defaultNetworkLatency * 0.25, function () {
        subtitle(
          "<h2>Deux noeuds commencent tous les deux une élection pour le même mandat...</h2>"
        );
      })
      .after(model().defaultNetworkLatency * 0.75, function () {
        subtitle(
          "<h2>...et chacun atteint un seul follower avant l'autre</h2>"
        );
      })
      .after(model().defaultNetworkLatency, function () {
        subtitle(
          "<h2>Désormais, chaque candidat a 2 voix et ne peut plus en recevoir pour ce mandat.</h2>"
        );
      })
      .after(1, function () {
        subtitle(
          "<h2>Les nœuds attendront une nouvelle élection et réessayeront.</h2>",
          false
        );
      })
      .at(model(), "stateChange", function (event) {
        return event.target.state() === "leader";
      })
      .after(1, function () {
        model().resetLatencies();
        subtitle(
          "<h2>Le noeud " +
            model().leader().id +
            " a reçu une majorité de voix pour le mandat " +
            model().leader().currentTerm() +
            " donc il devient leader.</h2>",
          false
        );
      })
      .after(1, wait)
      .indefinite()

      .then(function () {
        player.next();
      });

    player.play();
  };
});
