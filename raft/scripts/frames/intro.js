"use strict";
/*jslint browser: true, nomen: true*/
/*global define*/

define([], function () {
  return function (frame) {
    var player = frame.player(),
      layout = frame.layout(),
      model = function () {
        return frame.model();
      },
      client = function (id) {
        return frame.model().clients.find(id);
      },
      node = function (id) {
        return frame.model().nodes.find(id);
      },
      wait = function () {
        var self = this;
        model().controls.show(function () {
          self.stop();
        });
      };

    frame
      .after(1, function () {
        model().nodeLabelVisible = false;
        frame.snapshot();
        frame.model().clear();
        layout.invalidate();
      })

      .after(1000, function () {
        frame.model().title =
          "<h2 style=\"visibility:visible\">Qu'est-ce qu'un consensus distribué ?</h2>" +
          '<h3 style="visibility:hidden;">Commençons par un exemple...</h3>' +
          "<br/>" +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(1000, function () {
        layout.fadeIn($(".title h3"));
      })
      .after(1000, function () {
        frame.model().controls.show();
      })
      .after(50, function () {
        frame.model().title = frame.model().subtitle = "";
        layout.invalidate();
      })

      .after(800, function () {
        frame.snapshot();
        frame.model().subtitle =
          "<h2>Imaginons que nous avons un seul noeud dans notre système</h2>" +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(500, function () {
        frame.model().nodes.create("a");
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(100, function () {
        frame.snapshot();
        frame.model().subtitle = "";
        frame.model().zoom([node("a")]);
        layout.invalidate();
      })
      .after(600, function () {
        frame.model().subtitle =
          '<h3>Par exemple, vous pouvez considérer notre <span style="color:steelblue">noeud</span> comme une base de données qui stocke une seule valeur.</h3>' +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        node("a")._value = "x";
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(100, function () {
        frame.snapshot();
        frame.model().subtitle = "";
        frame.model().zoom(null);
        layout.invalidate();
      })
      .after(1000, function () {
        frame.model().subtitle =
          '<h3>Nous avons aussi un <span style="color:green">client</span> qui peut envoyer une valeur au serveur.</h3>' +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(500, function () {
        frame.model().clients.create("X");
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(100, function () {
        frame.snapshot();
        frame.model().subtitle += "";
        client("X").value("8");
        layout.invalidate();
      })
      .after(200, function () {
        frame.model().send(client("X"), node("a"), null, function () {
          node("a")._value = "8";
          layout.invalidate();
        });
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.model().subtitle =
          "<h3>Il est facile de parvenir à un accord ou à un <em>consensus</em> sur cette valeur avec un seul noeud.</h3>" +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(100, function () {
        frame.snapshot();
        frame.model().subtitle =
          "<h3>Mais comment arriver à un consensus si nous avons plusieurs noeuds ?</h3>" +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(500, function () {
        frame.model().nodes.create("b");
        layout.invalidate();
      })
      .after(500, function () {
        frame.model().nodes.create("c");
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(100, function () {
        frame.snapshot();
        frame.model().subtitle =
          "<h3>C'est le problème du <em>consensus distribué</em>.</h3>" +
          frame.model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(300, function () {
        frame.snapshot();
        player.next();
      });

    frame.addEventListener("end", function () {
      frame.model().title = frame.model().subtitle = "";
      layout.invalidate();
    });

    player.play();
  };
});
