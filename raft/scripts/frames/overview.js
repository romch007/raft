"use strict";
/*jslint browser: true, nomen: true*/
/*global define*/

define(["../model/log_entry"], function (LogEntry) {
  return function (frame) {
    var player = frame.player(),
      layout = frame.layout(),
      model = function () {
        return frame.model();
      },
      client = function (id) {
        return frame.model().clients.find(id);
      },
      node = function (id) {
        return frame.model().nodes.find(id);
      },
      wait = function () {
        var self = this;
        model().controls.show(function () {
          player.play();
          self.stop();
        });
      };

    frame
      .after(1, function () {
        model().nodeLabelVisible = false;
        model().clear();
        model().nodes.create("a");
        model().nodes.create("b");
        model().nodes.create("c");
        layout.invalidate();
      })

      .after(800, function () {
        model().subtitle =
          "<h2><em>Raft</em> est un protocol de mise en oeuvre de consensus distribué.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Regardons comment cela fonctionne.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(100, function () {
        frame.snapshot();
        model().zoom([node("b")]);
        model().subtitle =
          "<h2>Un noeud peut être dans trois états :</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        node("b")._state = "follower";
        model().subtitle =
          "<h2>L'état <em>Follower</em>,</h2>" + model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        node("b")._state = "candidate";
        model().subtitle =
          "<h2>L'état <em>Candidat</em>,</h2>" + model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        node("b")._state = "leader";
        model().subtitle =
          "<h2>ou l'état <em>Leader</em></h2>" + model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(300, function () {
        frame.snapshot();
        model().zoom(null);
        node("b")._state = "follower";
        model().subtitle =
          "<h2>Tous nos noeuds commencent dans l'état Follower.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Si les followers n'entendent pas de leader, ils peuvent devenir candidat</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, function () {
        node("a")._state = "candidate";
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Le candidat demande ensuite aux autres noeuds de voter.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, function () {
        model().send(node("a"), node("b"), { type: "RVREQ" });
        model().send(node("a"), node("c"), { type: "RVREQ" });
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Les noeuds répondent avec leur vote.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(300, function () {
        model().send(node("b"), node("a"), { type: "RVRSP" }, function () {
          node("a")._state = "leader";
          layout.invalidate();
        });
        model().send(node("c"), node("a"), { type: "RVRSP" }, function () {
          node("a")._state = "leader";
          layout.invalidate();
        });
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Le candidat devient leader s'il obtient les votes d'une majorité de noeuds.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Ce processus est appelé <em>Election de Leader</em>.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Tous les changements apportés au système passent maintenant par le leader.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle += " ";
        model().clients.create("x");
        layout.invalidate();
      })
      .after(1000, function () {
        client("x")._value = "5";
        layout.invalidate();
      })
      .after(500, function () {
        model().send(client("x"), node("a"), null, function () {
          node("a")._log.push(new LogEntry(model(), 1, 1, "SET 5"));
          layout.invalidate();
        });
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Chaque modification est ajoutée en tant qu'entrée dans le log du noeud.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Cette entrée de journal n'est actuellement pas validée et ne mettra pas à jour la valeur du noeud.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(300, function () {
        frame.snapshot();
        model().send(node("a"), node("b"), { type: "AEREQ" }, function () {
          node("b")._log.push(new LogEntry(model(), 1, 1, "SET 5"));
          layout.invalidate();
        });
        model().send(node("a"), node("c"), { type: "AEREQ" }, function () {
          node("c")._log.push(new LogEntry(model(), 1, 1, "SET 5"));
          layout.invalidate();
        });
        model().subtitle =
          "<h2>Pour valider l'entrée, le noeud la réplique d'abord sur les noeuds followers...</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().send(node("b"), node("a"), { type: "AEREQ" }, function () {
          node("a")._commitIndex = 1;
          node("a")._value = "5";
          layout.invalidate();
        });
        model().send(node("c"), node("a"), { type: "AEREQ" });
        model().subtitle =
          "<h2>puis le leader attend qu'une majorité de nœuds ait écrit l'entrée.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(1000, function () {
        node("a")._commitIndex = 1;
        node("a")._value = "5";
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>L'entrée est maintenant validée sur le nœud principal et l'état du nœud est 5.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().send(node("a"), node("b"), { type: "AEREQ" }, function () {
          node("b")._value = "5";
          node("b")._commitIndex = 1;
          layout.invalidate();
        });
        model().send(node("a"), node("c"), { type: "AEREQ" }, function () {
          node("c")._value = "5";
          node("c")._commitIndex = 1;
          layout.invalidate();
        });
        model().subtitle =
          "<h2>Le leader informe ensuite les followers que l'entrée est validée.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()
      .after(100, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Le cluster est maintenant parvenu à un consensus sur l'état du système.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(300, function () {
        frame.snapshot();
        model().subtitle =
          "<h2>Ce processus est appelé <em>Réplication de Log</em>.</h2>" +
          model().controls.html();
        layout.invalidate();
      })
      .after(100, wait)
      .indefinite()

      .after(300, function () {
        frame.snapshot();
        player.next();
      });

    player.play();
  };
});
